"use strict";
const fs = require("fs");
const express = require("express");

const videoGenerator = require("./video-generator");

const PORT = process.env.PORT || 8080;
const HOST = process.env.HOST || "0.0.0.0";
const PATH_IMGS = "images"

const app = express();
app.get("/", async (req, res) => {
  try {
    const videoPath = await videoGenerator.generate(PATH_IMGS);
    const stat = fs.statSync(videoPath);
    const filename = videoPath.toString().split("/").pop();
    console.log("Filename", filename);

    res.writeHead(200, {
      "Content-Type": "application/octet-stream",
      "Content-Disposition": "attachment; filename=" + filename,
      "Content-Length": stat.size,
    });
    fs.createReadStream(videoPath).pipe(res);
  } catch (e) {
    res.send(`:error ${e}`);
  }
});

app.listen(PORT, HOST, () => console.log(`Running on http://${HOST}:${PORT}`));
