#!/bin/bash
#http://ffmpeg.org/faq.html#How-do-I-encode-single-pictures-into-movies_003f
PATH_IMGS=$1
CODEC=$2
BITRATE=$3
IMG_EXT=$4
if [ -z $PATH_IMGS ]; then
    printf "%s forneça o path do diretorio que contem as imagens." >&2
    exit 1
fi

if [ -z $CODEC ]; then
    CODEC="mpeg4"
fi

if [ -z $BITRATE ]; then
    BITRATE="2500k"
fi

if [ -z $IMG_EXT ]; then
    IMG_EXT=".jpeg"
fi

files=($PATH_IMGS/*$IMG_EXT)
total=${#files[@]}

if [ $total -ne 720 ]; then
    echo "O numero de imagens deve ser exatmente igual a 720. Vc forneceu $total imagen(s)"
    exit
fi

# i=0
# for f in "${files[@]}"; do
#     i=$((i + 1))
#     file_number=$(printf "%03d" $i)
#     cp "$f" "tmp/img_$file_number$IMG_EXT"
# done

printf "$total imagens prontas!\n"
printf "criando video com 12fps e resolução 1920x1080...\n"

# ffmpeg -r 12 -f image2 -s 1920x1080 -i tmp/img_%03d$IMG_EXT -vcodec $CODEC -pix_fmt yuv420p videos/video_$BITRATE.mp4
cat $PATH_IMGS/*.jpeg | ffmpeg -nostats -loglevel 12 -y -r 8 -f image2pipe -s 1920x1080 -i - -vcodec $CODEC -pix_fmt yuv420p videos/video_$CODEC-$BITRATE.mp4

if [ $? -eq 0 ]; then
    printf "$(pwd)/videos/video_$CODEC-$BITRATE.mp4"
    exit 0
else
    printf "%s ocorreu um erro na conversão, duarnte a execução do script ffmpeg" >&2
    exit 1
fi
# end error handler
