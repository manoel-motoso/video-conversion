const spawn = require("child_process").spawn;
const scriptCoversor = "./script-conversor.sh";

const CODEC = "mpeg4"; //$2
const BITRATE = "2500k"; //$3
const IMG_EXT = ".jpeg"; //$4

const generate = (iamgePath) =>
  new Promise((ok, fail) => {
    const proc = spawn(scriptCoversor, [iamgePath, CODEC, BITRATE, IMG_EXT]);
    let msg = "";

    proc.stderr.setEncoding("utf8");
    proc.stdout.setEncoding("utf8");

    proc.stdout.on("data", (data) => {
      msg = data;
    });

    proc.stderr.on("data", (data) => {
      console.error(`stderr: ${data}`);
      msg = data;
    });

    proc.on("close", (code) => {
      switch (code) {
        case 1:
          console.log("ocorreu um erro", msg);
          fail(msg);
          break;
        case 0:
          console.log("ok", msg);
          ok(msg);
          break;
      }
    });
  });

module.exports = {
  generate,
};
